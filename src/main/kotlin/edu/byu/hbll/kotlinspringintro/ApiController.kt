package edu.byu.hbll.kotlinspringintro

import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class ApiController {

    @GetMapping("", produces = [MediaType.TEXT_PLAIN_VALUE])
    fun helloWorld(): String {
        return "Hello, world."
    }

}