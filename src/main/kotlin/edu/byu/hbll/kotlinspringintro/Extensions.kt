package edu.byu.hbll.kotlinspringintro

import com.fasterxml.jackson.databind.JsonNode

/**
 * The Cannon Menu API is...odd.  Rather that attach an attribute to each dish explaining what type of offering it is,
 * actual dishes are created and marked as "header" dishes.  All subsequent dishes are considered to be types of that
 * header dish until a new header is read.
 *
 * Converting from that structure to a Menu object is non-trivial, so for the sake of time, the code to do is included
 * for you as an extension to JsonNode.
 */
/*
fun JsonNode.asMenu(displayableHeaders: Collection<String> = setOf("ENTREES", "SOUPS")): Menu {
    val stationName = this.path("name").asText()
    val menuItems = mutableListOf<String>()

    var display = false
    for (menuItem in this.path("menu_items")) {
        val name = menuItem.path("name").asText()
        if (menuItem.path("header").asBoolean()) {
            display = displayableHeaders.contains(name)
        } else if (display) {
            menuItems.add(name)
        }
    }

    return Menu(stationName, menuItems)
}
*/
