package edu.byu.hbll.kotlinspringintro

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.test.web.client.getForObject

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class ApiControllerIT(@Autowired val app: TestRestTemplate) {

    @Test
    fun `helloWorld should return the correct response`() {
        // GIVEN a running instance of the application...
        // (assumed via dependency injection)

        // WHEN a GET request is sent to root path of the application...
        val response = app.getForObject<String>("/")

        // THEN the response should simply be the string "Hello, world.".
        assertThat(response).isEqualTo("Hello, world.")
    }

}
